<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @package Kingletas
 * @subpackage Kingletas_Customer
 * @category Features
 *
 * @author: Luis E. Tineo <magento@kingletas.com>
 * @copyright  Copyright (c) 2017  Kingletas (http://www.kingletas.com)
 */
 
class Kingletas_Customer_Block_Extra extends Mage_Core_Block_Template {


    public function getRatio() {
       return Mage::helper('kingletas_customer')->getRatio();
    }

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('kingletas/customer/extra.phtml');
    }

    public function getAjaxUrl() {
        return Mage::helper('kingletas_customer')->getAjaxUrl();
    }

}
