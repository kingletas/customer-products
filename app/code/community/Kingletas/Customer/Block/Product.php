<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @package Kingletas
 * @subpackage Kingletas_Customer
 * @category Features
 *
 * @author: Luis E. Tineo <magento@kingletas.com>
 * @copyright  Copyright (c) 2017  Kingletas (http://www.kingletas.com)
 */

class Kingletas_Customer_Block_Product extends Mage_Core_Block_Template {

    protected $filters = array();

    /**
     * Helper Container
     * @var \Kingletas_Customer_Helper_Data
     */
    protected $helper;

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('kingletas/customer/products.phtml');
        $this->helper = Mage::helper('kingletas_customer');
    }

    /**
     * Filters setter
     * @param array $filters
     * @return \Kingletas_Customer_Block_Product
     */
    public function setFilters(array $filters = array()) {
        $this->filters = $filters;
        return $this;
    }

    /**
     * Filters Getter
     * @return array
     */
    public function getFilters() {
        return $this->filters;
    }

    public function getProducts() {
        /**
         * Retrieve loaded category collection
         *
         * @return Mage_Eav_Model_Entity_Collection_Abstract
         */
        $filters = $this->getFilters();

        /**
         * build minimum set of filters
         */
        if (!isset($filters['sort'])) {
            $filters['sort'] = $this->helper->getSort();
        }
        /**
         * @var Mage_Catalog_Model_Resource_Product_Collection 
         */
        $products = $this->getProductCollection();

        $this->helper->addConditionsToCollection($products);
        $this->helper->addQty($products);
        /**
         * Sort the collection
         */
        $products->setOrder('price', $filters['sort']);
        $this->helper->callableFilters($products);
        $this->helper->addPriceFilter($products, $filters);


        return $products;
    }

    /**
     * Gets a product collection
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection() {
        /**
         * Get a product collection with default attributes
         */
        $products = Mage::getModel("catalog/product")->getCollection();

        $products->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents();

        return $products;
    }

}
