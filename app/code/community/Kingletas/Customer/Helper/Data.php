<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @package Kingletas
 * @subpackage Kingletas_Customer
 * @category Features
 *
 * @author Luis E. Tineo <magento@kingletas.com>
 * @copyright  Copyright (c) 2017  Kingletas (http://www.kingletas.com)
 */

class Kingletas_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     *
     * @var \Mage_Core_Model_Config
     */
    public static $config;

    const CUSTOM_FILTERS_PATH = 'global/kingletas/filters';
    const PRODUCT_TYPES_PATH = 'global/kingletas/products';
    const DEFAULT_SORT_PATH = 'global/kingletas/defaults/sort';
    const DEFAULT_LIMIT_PATH = 'global/kingletas/defaults/limit';
    const DEFAULT_RATIO_PATH = 'global/kingletas/defaults/ratio';

    /**
     * Get the ratio for the high and low ranges
     * @return int
     */
    public function getRatio()
    {
        return (int) $this->config->getNode(self::DEFAULT_RATIO_PATH);
    }

    public function __construct()
    {
        $this->config = Mage::app()->getConfig();
    }

    /**
     * Gets the hard limit value
     * @return int
     */
    public function getLimit()
    {
        return (int) $this->config->getNode(self::DEFAULT_LIMIT_PATH);
    }

    /**
     * Gets the default sort
     * @return string
     */
    public function getSort()
    {
        return (string) $this->config->getNode(self::DEFAULT_SORT_PATH);
    }

    /**
     *
     * Ping connections
     * @return string
     */
    public function ping()
    {
        return 'pong';
    }

    /**
     * Includes the AccountController where needed
     * @return \Kingletas_Customer_Helper_Data
     */
    public function getIncludeAccountController()
    {
        $this->getIncludePath('Mage_Customer', 'AccountController', 'controllers');
        return $this;
    }

    /**
     * Builds and includes files required
     * @param string $module
     * @param string $file
     * @param string $type
     * @return string
     */
    protected function getIncludePath($module, $file, $type)
    {
        /**
         * IF filename exists
         * THEN include it
         */
        $filename = $this->config->getModuleDir($type, $module) . DS . $file . ".php";
        $include = file_exists($filename);
        if ($include) {
            include_once $filename;
        }
        return $include;
    }

    /**
     *
     * @param array $postData
     * @return array
     */
    public function validatePostData(array $postData)
    {
        $result = array('result' => true, 'message' => array());
        /**
         * IF postData is a valid array
         * THEN verify the sort, low and high values
         */
        if (empty($postData)) {
            $result['result'] = false;
            $result['message'] = [$this->__('Data is malformed -- expected an array')];
        } else if (!isset($postData['low']) || !isset($postData['high']) || !isset($postData['sort'])) {
            $result['result'] = false;
            $result['message'] = [$this->__('Data is malformed -- expected an array with range values')];
        } else {
            $sorts = array(Zend_Db_Select::SQL_DESC, Zend_Db_Select::SQL_ASC);
            foreach ($postData as $key => $value) {
                if (in_array($key, array('low', 'high'))) {
                    $postData[$key] = (float) $value;
                }
                /**
                 * ensure sort has been defined and is one of the allowed values
                 */
                if ($key === 'sort' && !in_array($value, $sorts)) {
                    $result['result'] = false;
                    $result['message'][] = $this->__("Sort was not passed correctly");
                }
            }
            /**
             * ensure we don't divide by zero
             */
            if ($e = ((float) $postData['low'] !== 0)) {
                $e = ((float) $postData['high'] / (float) $postData['low']) <= $this->getRatio();
            }

            if (!$e) {
                $result['result'] = $e;
                $result['message'][] = $this->getRatioErrorMessage();
            }
        }

        return $result;
    }

    /**
     * Range error message used when the ratio is great than the defined allowed ratio
     *
     * @return string
     */
    public function getRatioErrorMessage()
    {
        return $this->__(sprintf('High & Low Range should have a %d ratio or less', $this->getRatio()));
    }

    /**
     * Gets the ajax post action url
     * @return string
     */
    public function getAjaxUrl()
    {
        return Mage::getModel('core/url')->getUrl('kingletas/product/ajax');
    }

    /**
     * Gets the allowed product types to use with the collection
     *
     * @return array
     */
    public function getProductTypes()
    {
        $pTypes = array();
        /**
         * IF the product type node has been defined
         * THEN get the product types
         */
        foreach ($this->config->getNode(self::PRODUCT_TYPES_PATH) as $types) {
            foreach ($types as $type) {
                $pTypes[] = (string) $type;
            }
        }
        return $pTypes;
    }

    /**
     * Joins the Stock item table to add the qty column to the select
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $products
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function addQty(Mage_Catalog_Model_Resource_Product_Collection $products)
    {
        /**
         * WHEN given a product collection join the cataloginventory/stock_item table
         */
        $products->getSelect()->join(array('si' => $products->getTable('cataloginventory/stock_item')), 'e.entity_id=si.product_id', array('qty' => 'qty'));

        return $products;
    }

    /**
     * Adds the price limits and limits the collection
     * @param Mage_Catalog_Model_Resource_Product_Collection $products
     * @param array $filters
     *
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function addPriceFilter(Mage_Catalog_Model_Resource_Product_Collection $products, array $filters)
    {
        /**
         * IF the low and high range prices are defined
         * THEN apply the condition to the collection
         * ELSE do nothing
         */
        if (isset($filters['low']) && isset($filters['high'])) {
            $products->getSelect()->where(
                sprintf(" `price_index`.`price` BETWEEN %f AND %f", $filters['low'], $filters['high'])
            );
        }

        return $products;
    }

    /**
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $products
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function callableFilters(Mage_Catalog_Model_Resource_Product_Collection $products)
    {
        /**
         * IF filters have been defined
         * THEN check each class implements the correct interface
         * WHERE possible apply the flter to the given collection
         */
        foreach ($this->config->getNode(self::CUSTOM_FILTERS_PATH) as $filters) {
            foreach ($filters as $filter) {
                /**
                 * IF the class implements the interface then
                 * THEN the filter method should be callable
                 */
                $filter = Mage::getModel((string) $filter->class);
                if ($this->implemetsInterface(get_class($filter))) {
                    $filter->filter($products);
                }
            }
        }

        return $products;
    }
    /**
     * Verifies that an interface has been implemented by the given class
     *
     * @author  Luis E. Tineo <luis@kingletas.com>
     * @param  string     $class
     * @param  string     $interface
     * @return bool
     */
    protected function implemetsInterface($class, $interface = 'Kingletas_Customer_Model_Interface')
    {
        return interface_exists($interface) && in_array($interface, class_implements($class));
    }
    /**
     * Adds the following conditions to the collections:
     *     - Status
     *     - Stock
     *     - visibility in the the site
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function addConditionsToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInSiteFilterToCollection($collection);

        return $collection;
    }

}

