<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @package Kingletas
 * @subpackage Kingletas_Customer
 * @category Features
 *
 * @author: Luis E. Tineo <magento@kingletas.com>
 * @copyright  Copyright (c) 2017  Kingletas (http://www.kingletas.com)
 */
 
class Kingletas_Customer_Model_Filter_Status implements Kingletas_Customer_Model_Interface {

    /**
     * Adds the status filter to the collection
     * 
     * @see \Kingletas_Customer_Helper_Data::callableFilters
     * 
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function filter(Mage_Catalog_Model_Resource_Product_Collection $collection) {
        $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        return $collection;
    }

}
