<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @package Kingletas
 * @subpackage Kingletas_Customer
 * @category Features
 *
 * @author: Luis E. Tineo <magento@kingletas.com>
 * @copyright  Copyright (c) 2017  Kingletas (http://www.kingletas.com)
 */
 
Mage::helper('kingletas_customer')->getIncludeAccountController();

class Kingletas_Customer_ProductController extends Mage_Customer_AccountController {

    public function indexAction() {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $object = new Varien_Object(array('block_name' => 'kingletas_customer/extra'));

        Mage::dispatchEvent('kingletas_customer_content_before', array('object' => $object));

        $this->getLayout()->getBlock('content')->append(
                $this->getLayout()->createBlock($object->getData('block_name'))
        );
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Products'));
        $this->renderLayout();
    }

    public function ajaxAction() {
        if ($this->getRequest()->isAjax() or $this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getParam('range');
            //$postData['low'] = 100;
            //$postData['high'] = 1000;
            //$postData['sort'] = 'sds';
            if ($postData['low'] > $postData['high']) {
                $tmp = $postData['high'];
                $postData['high'] = $postData['low'];
                $postData['low'] = $tmp;
            }
            $validation = Mage::helper('kingletas_customer')->validatePostData($postData);
            $result = array('response' => '', 'error' => false, 'errorMsg' => '');
            if ((bool) $validation['result']) {
                $block = $this->getLayout()->createBlock('kingletas_customer/product')
                        ->setFilters($postData)
                        ->setTemplate('kingletas/customer/grid.phtml');

                $result['response'] = $block->toHtml();
            } else {
                $result['error'] = true;
                $result['errorMsg'] = $validation['message'];
            }
            $this->getResponse()
                    ->clearHeaders()
                    ->setHeader('content-type', 'application/json')
                    ->setBody(json_encode($result));
        }
    }

    public function pingAction() {
        return Mage::helper('kingletas_customer')->ping();
    }

}
