/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @package Kingletas
 * @subpackage Kingletas_Customer
 * @category Features
 *
 * @author: Luis E. Tineo <magento@kingletas.com>
 * @copyright  Copyright (c) 2017  Kingletas (http://www.kingletas.com)
 */
 
document.observe("dom:loaded", function () {
    $$('.clear-on-click').each(function (e) {
        $(e).on('focus', function (el) {
            el = $(el).findElement();
            if (el.value.indexOf('Enter your') !== -1) {
                el.value = "";
            }
        });
        $(e).on('blur', function (el) {
            el = $(el).findElement();
            if (el.value === "") {
                if (el.id.indexOf('low') !== -1)
                    el.value = extra.low_end_message;
                else if (el.id.indexOf('high') !== -1)
                    el.value = extra.high_end_message;
            }
        });
    });

    Validation.add('validate-range', extra.range_validation_message, function (v, elm) {
        //will add class validation later
        // $w(elm.className).each(function (name) { 
        var elems = $$(".validate-range");
        var low = parseNumber(elems[0].value);
        var high = parseNumber(elems[1].value);

        if (isNaN(low) || isNaN(high)) {
            return false;
        }
        if (low > high) {
            var tmp = low;
            low = high;
            high = tmp;
        }
        return ((high / low) * 1) <= extra.ratio;
        //  });
    });

    extra.doAjax = function () {

        if (extra.form.validator.validate()) {
            new Ajax.Updater(
                    {success: 'formSuccess'},
                    extra.postUrl, {
                        method: 'post',
                        asynchronous: true,
                        evalScripts: false,
                        onComplete: function (response) {
                            if (200 === response.status) {
                                var r = response.responseJSON;
                                if (r.error) {
                                    alert(r.errorMsg)
                                } else {
                                    $('customer-product-container').update(r.response);
                                }
                            } else {
                                //error handling here
                                alert('Please refresh your browser')
                            }
                        },
                        parameters: $(extra.formId).serialize(true),
                    }
            );
        }
    }

    new Event.observe(extra.formId, 'submit', function (e) {
        e.stop();
        extra.doAjax();
    });
});
